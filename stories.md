## Search event 1

* greet
  - utter_greet
* list
  - action_list
* detail {"eventID":1}
  - action_detail

## Search event 2

* list
  - action_list
* greet
  - utter_greet
* detail {"eventID":9}
  - action_detail

## Search event 3

* list
  - action_list
* detail{"eventID": 10}
  - action_detail
* greet
  - utter_greet