# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging
import requests
import json
from rasa_core_sdk import Action
from rasa_core_sdk.events import SlotSet

logger = logging.getLogger(__name__)


class ActionList(Action):
    def name(self):
        return "action_list"

    def run(self, dispatcher, tracker, domain):
        request = json.loads(
            requests.get("https://www.eventbriteapi.com/v3/events/search/?token=K7JT5YBAKTKXE3ZYYJNH&location.address=Paris&sort_by=date").text
        )

        dispatcher.utter_message("Events happening in Paris :")

        for i, event in enumerate(request["events"]):
           dispatcher.utter_message("["+str(i)+"]:" + event["name"]["text"])

        dispatcher.utter_message("\n\nType your event number to get more information about it\n\n")

        return [SlotSet("events", request["events"])]


class ActionDetail(Action):
    def name(self):
        return "action_detail"

    def run(self, dispatcher, tracker, domain):
        
        if not tracker.latest_message["entities"]:
            self.getFallBackMessage(dispatcher)
            return 

        eventID = tracker.latest_message["entities"][0]["value"]
        events = tracker.get_slot("events")

        if events is not None and int(eventID) < 50:
            dispatcher.utter_message("To get more information about : " + events[int(eventID)]["name"]["text"])
            dispatcher.utter_message("Visit : " + events[int(eventID)]["url"])
        else:
            self.getFallBackMessage(dispatcher)

    def getFallBackMessage(self, dispatcher):
        dispatcher.utter_message("\n\nType 'list' to get all the events happening in Paris !\n\n")
