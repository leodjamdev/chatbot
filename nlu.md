## intent:greet
- hey
- hello
- hi
- good morning
- good evening
- hey there

## intent:list
- list
- events
- event
- paris
- paris event
- paris events
- get events
- get event
- list event

## intent:detail
- [1](eventID)
- event [1](eventID)
- event[1](eventID)

## intent:detail
- [10](eventID)
- event [10](eventID)
- event[10](eventID)

## intent:detail
- [25](eventID)
- event [25](eventID)
- event[25](eventID)

## intent:detail
- [38](eventID)
- event [38](eventID)
- event[38](eventID)

## intent:detail
- [43](eventID)
- event [43](eventID)
- event[43](eventID)

## regex:eventID
- ^[1-9][0-9]?$|^49$